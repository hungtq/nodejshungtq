
'use strict'

const SocketIO = require('socket.io')
const UserController = require('../app/Controllers/Ws/UserController')
const userController = new UserController()

module.exports = (server) => {
  const io = SocketIO(server, {
    transport: ['websocket'],
    pingTimeout: 3000,
    pingInterval: 1000
  })

  io.on('connection', (socket) => {
    console.log(`connect ${socket.id} ok`)

    userController.getUser(io, socket)

    socket.on('disconnect', () => {
      console.log(`user ${socket.id} disconnected`)
      io.emit(`user ${socket.id} disconnected`)
    })
  })
}

'use strict'

const express = require('express')
const authController = require('../app/Controllers/Api/AuthController')
const jsonSuccess = require('../app/Responses/JsonSuccess')
const router = express.Router()

/** GET api/v1/health-check - Check service health */
router.get('/health-check', (req, res) => jsonSuccess(res))
router.get('/health-check-api', (req, res) => jsonSuccess(res))

/** POST api/v1/login - Login Authenication */
router.route('/login').post(authController.login)
router.route('/register').post(authController.register)

module.exports = router

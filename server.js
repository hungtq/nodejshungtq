require('dotenv').config()
const createError = require('http-errors')
const express = require('express')
const http = require('http')
const cookieParser = require('cookie-parser')
const logger = require('morgan')
const routes = require('./routes/api')
const cors = require('cors')
const bodyParser = require('body-parser')
const helmet = require('helmet')
const app = express()
const server = http.Server(app)
const port = process.env.PORT || 3000
const env = process.env.NODE_ENV || 'development'
const winstonLogger = require('./app/Logger/appLog')
const limiter = require('././config/limitRequest')

winstonLogger.writeLog('start server', 'start server')

require('./config/databaseMongo') // connect mongodb

const exampleJob = require('./app/Tasks/exampleTask')

exampleJob.start()

server.listen(port, () => {
  console.info(`
=====================================================
😘 Server 🏃 (running...) on Port:${port}***${env} 😘
=====================================================
  `)
})

app.enable('trust proxy') // only if you're behind a reverse proxy (Heroku, Bluemix, AWS ELB, Nginx, etc)
app.use(limiter) //  apply to all requests
app.use(helmet()) // set header
app.use(logger('dev'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(cookieParser())

app.use(cors('*')) // enable CORS - Cross Origin Resource Sharing

app.use('/api/v1', routes)
require('./routes/socket')(server)

// catch 404 and forward to error handler
app.use((req, res, next) => {
  next(createError(404))
})

// error handler
app.use((err, req, res, next) => {
  // set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}

  // render the error page
  res.status(err.status || 500)
  res.render('error')
})

module.exports = app

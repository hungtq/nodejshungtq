const mongoose = require('mongoose')
const Fawn = require('fawn')

mongoose.Promise = global.Promise

// connect to mongo db
const mongoUri = 'mongodb://127.0.0.1:27017/testDB'
mongoose.connect(
  mongoUri,
  {}
)
mongoose.connection.on('error', err => {
  console.error(`⚡️ 🚨 ⚡️ 🚨 ⚡️ 🚨 ⚡️ 🚨 ⚡️ 🚨  → ${err.message}`)
  throw err
})

Fawn.init(mongoose)
console.info('connected mongodb')

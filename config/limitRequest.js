const rateLimit = require('express-rate-limit')

const Limiter = rateLimit({
  windowMs: 1 * 60 * 1000, // 1 minutes
  max: 100, // limit each IP to 100 requests per windowMs
  message: `Too many request from this IP, please try again after`
})

module.exports = Limiter

# Nodejs Style Guide - Convention
-------------------------------------
## 1. Format
### 1.1 Sử dụng 2 space
### 1.2 Newlines
 - ```sử dụng UNIX-style newlines (\n) ```

### 1.3 Không có khoảng trống ở cuối(No trailing whitespace)

### 1.4 Không Sử dụng dấu chấm phẩy(Semicolons)
### 1.5 80 ký tự trên 1 dòng
### 1.6 Sử dụng dấu nháy đơn (ngoại trừ viết JSON)

#### bad:

  - ``` let name = "quoctv" ```

#### good:

  - ``` let name = 'quoctv' ```

### 1.7 Sử dụng whitespace sau keywords
#### bad:

  - ``` if(condition) { ... } ```

#### good:

  - ``` if (condition) { ... } ```

### 1.8 Sử dụng whitespace sau tên function
#### bad:

  - ``` function name(arg) { ... }
     run(function() { ... })  ```

#### good:

  - ``` function name (arg) { ... }
     run(function () { ... })  ```

### 1.9 Dấu ngoặc nhọn (Curly braces)
  - Giữ các câu lệnh khác trên cùng một dòng với dấu ngoặc nhọn.

#### bad:

```sh
  if (true)
  {
    console.log('bad')
  }
  -------------------------------------

  if (condition) {
    // ...
  } 
  else {
    // ...
  }
```
#### good:

```sh
  if (true) {
    console.log('good');
  } 
  -------------------------------------
  if (condition) {
    // ...
  } else {
    // ...
  }
```
### 1.10 Khai báo mỗi biến trên 1 câu lệnh
#### bad:

  
```sh
  let keys = ['foo', 'bar'],
      values = [23, 42],
      object = {},
      key;

  while (keys.length) {
    key = keys.pop();
    object[key] = values.pop();
  }  
```

#### good:

```sh
  let keys   = ['foo', 'bar'];
  let values = [23, 42];
  let object = {};

  while (keys.length) {
    let key = keys.pop();
    object[key] = values.pop();
  }
```
- Khai báo một biến cho mỗi câu lệnh , nó giúp dễ dàng sắp xếp lại các dòng.

--------------------------------------------------------------------------

## 2. Đặt tên hàm và biến (Naming Conventions)
### 2.1 Sử dụng UPPERCASE(chữ in hoa) cho hằng số
  - Các hằng số phải được khai báo là các biến thông thường hoặc các thuộc tính lớp tĩnh, sử dụng tất cả các chữ hoa.

#### bad:

  ```sh
  const hours = 24
  const daysInMonth = 30
```

#### good:

  ```sh
  const HOURS = 24
  const DAYS_IN_MONTH = 30
```
### 2.2 Sử dụng UCC(UpperCamelCase) cho class name

#### bad:

  ```sh
  class mailService() {
    //something;
  } 
```

#### good:

  ```sh
  class MailService() {
    //something;
  } 
```
### 2.3 Sử dụng LCC(LowerCamelCase) cho biến (Variables), thuộc tính(properties) và tên hàm(function)
 - Các biến, thuộc tính và tên hàm nên sử dụng lowerCamelCase. 
 - Chúng cũng nên mang tính mô tả. 
 - Các biến ký tự đơn và các từ viết tắt không phổ biến nói chung nên tránh.

#### bad:

  ``` let total_item ```

#### good:

  ``` let totalItem ```
### 2.4 Không đặt tên biến lặp với lớp thể hiện của nó

#### bad:

  ```sh
  const Car = {
    carMake: 'Honda',
    carModel: 'Accord',
    carColor: 'Blue'
  }

  function paintCar(car) {
    car.carColor = 'Red'
  }
```

#### good:

  ```sh
  const Car = {
    make: 'Honda',
    model: 'Accord',
    color: 'Blue'
  }

  function paintCar(car) {
    car.color = 'Red'
  }
```
--------------------------------------------------------------------------

## 3. Điều kiện (Conditionals)
### 3.1 Sử dụng toán tử === và !==.
  - Luôn sử dụng toán tử === thay cho == .
  - Lưu ý: đối với object có thể dùng toán tử == để kiểm tra cho null hoặc undefined.

#### bad:

  ```sh
  if (name != 'quoctv')
  if (name == 'quoctv')
  ```

#### good:

  ```sh
  if (name !== 'quoctv')
  if (name === 'quoctv')
  ```
### 3.2 Kiểm tra với nhiều điều kiện.

#### bad:

  ```sh
    if (password.length >= 6 && /^(?=.*\d).{6,}$/.test(password)) {
      console.log('bad');
    }
  ```

#### good:

  ```sh
    let isValidPassword = password.length >= 6 && /^(?=.*\d).{6,}$/.test(password);

    if (isValidPassword) {
      console.log('good');
    }
  ```
### 3.3 Tránh điều kiện Yoda(https://en.wikipedia.org/wiki/Yoda_conditions)
```sh
  if (25 === age) { }    // ✗ avoid
  if (age === 25) { }    // ✓ ok
```
### 3.4 Khi sử dụng toán tử 3 điều kiện （ ? : ）
```sh
  weather = sun.shin? ?
    'well'
    :
    'cloud'   // ✗ avoid

  weather = sun.shiny? ? 'well' : 'cloud' // ✓ ok
```
### 3.5 Không được sử dụng các toán tử 3 điều kiện lồng nhau
```sh
  some_condition ? (nested_condition ? nested_something : nested_something_else) : something_else // ✗ avoid

  if (some_condition) {
    nested_condition ? nested_something : nested_something_else
  } else {
    something_else 
  } // ✓ ok
```
--------------------------------------------------------------------------

## 4. Biến (variables)
### 4.1 Khai báo biến object hoặc array.
  - Sử dụng dấu phẩy và đặt các khai báo ngắn trên một dòng .

#### bad:

  ```sh
  var a = [
    'hello', 'world'
  ]
  var b = {"good": 'code'
          , is generally: 'pretty'
          }
  ```

#### good:

  ```sh
  var a = ['hello', 'world']
  var b = {
    good: 'code',
    'is generally': 'pretty',
  }
  ```
### 4.2 Khai báo biến với hằng số.
  - Lí do: Thuận lợi cho việc tìm kiếm và sửa đổi sau này.

#### bad:

  ```sh
    setTimeout (function() {
      client.connect(afterConnect)
    }, 1000)

    function afterConnect() {
      console.log('good')
    }
  ```

#### good:

  ```sh
    const TIME_CONNECT= 1000
    setTimeout (function() {
      client.connect(afterConnect);
    }, TIME_CONNECT)

    function afterConnect() {
      console.log('good')
    }
  ```
--------------------------------------------------------------------------

## 5. Hàm (Functions)
### 5.1 Không sử dụng nested(lồng) closures function.
  - Lí do: function rõ ràng dễ đọc hơn.

#### bad:

  ```sh
  setTimeout (function() {
    client.connect(function() {
      console.log('bad')
    })
  }, 1000)
  ```

#### good:

  ```sh
  const TIME_CONNECT= 1000
  
  setTimeout (function() {
    client.connect(afterConnect)
  }, TIME_CONNECT)

  function afterConnect() {
    console.log('good')
  }
  ```
### 5.2 Tránh lồng câu lệnh điều kiện trong function.
  - Lí do: 
    - function rõ ràng dễ đọc hơn.
    - Kết quả xử lý trả về nhanh hơn.

#### bad:

  ```sh
  function isPercentage (val) {
    if (val >= 0) {
      if (val < 100) {
        return true
      } else {
        return false
      }
    } else {
      return false
    }
  }
  ```

#### good:

  ```sh
  function isPercentage (val) {
    if (val < 0) {
      return false
    }

    if (val > 100) {
      return false
    }

    return true
  }
  ```
### 5.3 Luôn xử lý tham số hàm err.
#### bad:
  ```sh
  run(function (err) {
    console.log('done')
  })
  ```

#### good:
  ```sh
  run(function (err) {
    if (err) throw err
    console.log('done')
  })
  ```
### 5.4 Sử dụng arrow function với ES6.
#### 5.4.1 Trong trường hợp nhiều tham số
#### bad:
  ```sh
  // ES5 
  var multiply = function(x, y) {
      return x * y
  }
  ```

#### good:
  ```sh
  // ES6 
  var multiply = (x, y) => { return x * y }
  ```
#### good best(đối với trường hợp 1 biểu thức như trên):
  ```sh
  // ES6 
  var multiply = (x, y) => x * y
  ```
#### 5.4.2 Trong trường hợp 1 tham số

#### bad:
  ```sh
  //ES5 
  var phraseSplitterEs5 = function phraseSplitter (phrase) { 
      return phrase.split(' ')
  }
  ```

#### good:
  ```sh
  //ES6 
  var phraseSplitterEs6 = phrase => phrase.split(" ")
  ```
#### 5.4.3 Trong trường hợp không có tham số

#### bad:
  ```sh
  //ES5 
  var docLogEs5 = function docLog() { 
      console.log('bad')
  }
  ```

#### good:
  ```sh
  //ES6 
  var docLogEs6 = () => { 
    console.log('good')
  } 
  ```
--------------------------------------------------------------------------

## 6. Chú Thích (Comment)
  - Sử dụng dấu // để chú thích.

#### bad:

  ```sh

  ```

#### good:

  ```sh

  ```
  --------------------------------------------------------------------------

# Quy định code trong project Nodejs
-------------------------------------
## 1. Controllers: 
  Chỉ xử lý logic và response
## 2. Model: 
### 2.1 NoSQL(Mongodb):
  - Định nghĩa schema
  - Định nghĩa 1 số phương thức callback model

### 2.2 SQL(mysql, postgres):
  - Thư mục Migrations: định nghĩa schema
  - Định nghĩa association model
  - Định nghĩa 1 số phương thức callback model, scope với model ...

## 3. Middleware: 

### 3.1 Policies:
  - Là nơi chặn các điều kiện với dữ liệu đầu vào với logic được yêu cầu của project

### 3.2 Validations:
  - Là nơi validate dữ liệu đầu vào
  
## 4. Repositories:

  - Là nơi để xử lý với dữ liệu.
  - Các `repositories` không được gọi chéo nhau
  - Repositories được phép sử dụng require từ `Models` `Services` `Utils` `Responses`

## 5. Tasks:
  - Xử lý các tác vụ cronjob

## 6. Services:
  - Là các đoạn mã sử dụng với SDK hoặc package
  - Các dịch vụ từ bên ngoài

## 7. Utils:
  - Là nơi chứa các function được định nghĩa dùng chung cho project. vd: tính phân trang...

## 8. config:
  -  chứa các file config của dự án

## 9. log
  - Chứa toàn bộ file log của dự án

## 10. routes:
  - định nghĩa routes.

--------------------------------------------------------------------------

# Một số quy định khác
-------------------------------------
## 1. Đặt tên file: ví dụ `AuthController.js`

## 2. Các giá trị không thay đổi cần khai báo là `const`

## 3. Sử dụng ES6

## 4. Phải đặt `use strict` cho tất cả các file js

## 5. Phải đặt `try/catch` và handling `error`

## 1. Require theo chuẩn : `const authController = require(path)`  


 
 

 






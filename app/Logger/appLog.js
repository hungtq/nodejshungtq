'use strict'

const winston = require('winston')
require('winston-papertrail')
const moment = require('moment')

module.exports = {
  writeLog (file, message) {
    try {
      if (!file || !message) {
        console.log('[LOGGER] Message & File is required!')
        return
      }
      let date = moment().format('YYYY-MM-DD')
      let filename

      filename = `app/log/${file}.${date}.log`

      // push log to https://papertrailapp.com/events
      const winstonPapertrail = new winston.transports.Papertrail({
        host: process.env.PAPERTRAIL_HOST,
        port: process.env.PAPERTRAIL_PORT
      })
      winstonPapertrail.on('error', err => {
        console.error(err)
        // Handle, report, or silently ignore connection errors and failures
      })

      const logger = winston.createLogger({
        level: 'info',
        format: winston.format.json(),
        transports: [
          new winston.transports.Console(),
          new winston.transports.File({
            filename: filename
          }),
          winstonPapertrail
        ]
      })
      logger.info(message)
    } catch (error) {
      throw error
    }
  }
}

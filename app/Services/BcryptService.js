'use strict'

const bcrypt = require('bcrypt-nodejs')

module.exports = {
  /**
     * Hash password
     * @param originPassword
     * @returns {Promise}
     */
  hash: originPassword => {
    const promise = new Promise((resolve, reject) => {
      bcrypt.genSalt(10, (err, salt) => {
        if (err) reject(err)
        bcrypt.hash(originPassword, salt, null, (newErr, hash) => {
          if (newErr) {
            console.log('BcryptService - hash', newErr)
            reject(newErr)
          } else resolve(hash)
        })
      })
    })
    return promise
  },

  compare: (origin, hash) => {
    return new Promise(function (resolve, reject) {
      bcrypt.compare(origin, hash, (err, result) => {
        if (err) {
          console.log('BcryptService - compare: ', err)
          reject(err)
        } else resolve(result)
      })
    })
  }
}

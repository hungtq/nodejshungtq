'use strict'

const fs = require('fs')
const aws = require('aws-sdk')

aws.config.update({
  accessKeyId: process.env.S3_KEY,
  secretAccessKey: process.env.S3_SECRET
})

aws.config.apiVersions = {
  s3: '2006-03-01'
  // other service API versions
}

const s3 = new aws.S3()

module.exports = {
  /**
       * @param filePath
       * @param fileName
       * @return {*}
       */
  uploadImage: (filePath, fileName) => {
    return new Promise(resolve => {
      const params = {
        Bucket: process.env.s3.bucket,
        Key: fileName,
        Body: fs.createReadStream(filePath)
      }

      s3.upload(params, function (err, data) {
        if (err) {
          resolve({ success: false, err })
        }
        resolve({ success: true, data })
      })
    })
  }
}

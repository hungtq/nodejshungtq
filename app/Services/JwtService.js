'use strict'

const jwt = require('jsonwebtoken')

module.exports = {
  /**
     * @param user
     * @return {*}
     */
  generateUserToken: user => {
    try {
      return jwt.sign(user, process.env.JWT_SECRET, { expiresIn: process.env.JWT_EXPIRED_IN })
    } catch (error) {
      throw error
    }
  },

  verifyToken: (token, secret = process.env.jwtSecret) => {
    return new Promise((resolve, reject) => {
      jwt.verify(token, secret, (error, decode) => {
        if (error) resolve(error)
        resolve(decode)
      })
    })
  }
}

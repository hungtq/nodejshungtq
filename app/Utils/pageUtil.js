'use strict'

module.exports = {
  /**
     * @param page
     * @return {number}
     */
  getSkipItemByPage: (page, paginateLimit = 10) => {
    return (page - 1) * paginateLimit
  },

  /**
     * Calculator total pages
     * @param totalDocument
     * @return {number}
     */
  calculatorTotalPages: (totalDocument, paginateLimit = 10) => {
    return Math.ceil(totalDocument / paginateLimit)
  }
}

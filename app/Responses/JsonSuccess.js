'use strict'

module.exports = (res, options = {}) => {
  const {
    data,
    message
  } = options

  return res.json({
    status: 200,
    data: data || null,
    message: message || 'Success',
    success: true
  })
}

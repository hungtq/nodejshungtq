'use strict'

module.exports = (res, options = {}) => {
  const {
    error,
    message
  } = options

  return res.json({
    status: 200,
    error: error || null,
    message: message || 'Error',
    success: true
  })
}


'use strict'

const User = require('../Models/User')

class UserRepository {
  constructor () {
    this.user = User
  }

  /**
   * Get user by userId
   *
   * @param $userId
   * @returns {Promise<*>}
   */
  async getUser ($userId) {
    return this.user.find($userId)
  }
}

module.exports = UserRepository

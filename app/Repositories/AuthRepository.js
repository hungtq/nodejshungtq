
'use strict'

const User = require('../Models/User')
const BcryptService = require('../Services/BcryptService')

class AuthRepository {
  static async login (username, password) {
    try {
      // const user = await User.query().findOne(email).select(['id', 'email', 'password']) // mysql
      const user = await User.findOne({ username: username }) //mognodb
      if (!user) return null

      const dbPassword = user.password

      const isPasswordCorrect = await BcryptService.compare(
        password,
        dbPassword
      )
      if (!isPasswordCorrect) return null
      console.log(user);

      return user
    } catch (error) {
      throw error
    }
  }
  static async register (username, password) {
      try {
        const user = await User.findOne({ username: username })
        const isUsernameValid = username.length !== 0
        const isPasswordValid = password.length !== 0
        if (user == null) {
          if (!isUsernameValid || !isPasswordValid) {
            return null
          } else {
            return User.create({ username: username, password: password})
          }
        } else {
          const isUsernameExists = username === user.username
          if (!isUsernameExists) {
              if (!isUsernameValid || !isPasswordValid) {
              return null
            } else {
              return User.create({ username: username, password: password })
            }
          } else {
            return null
          }
        }
      } catch (error) {
        throw error
      }
    }
}

module.exports = AuthRepository

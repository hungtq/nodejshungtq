'use strict'
const mongoose = require('mongoose')
const Schema = mongoose.Schema
let RoomChat = new Schema(
  'RoomChats',
  { userId: Number,
    roomStatus: Boolean,
    massage: String })

module.exports = mongoose.model('RoomChats', RoomChat)

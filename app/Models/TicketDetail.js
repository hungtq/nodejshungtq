'use strict'
const mongoose = require('mongoose')
const Schema = mongoose.Schema
let TicketDetail = new Schema(
  'TicketDetails',
  { userId: Number,
    ticketId: Number,
    ticketAmount: Number,
    checkVip: Boolean,
    ticketName: String
  })

module.exports = mongoose.model('TicketDetails', TicketDetail)

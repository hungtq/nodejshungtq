'use strict'
const mongoose = require('mongoose')
const Schema = mongoose.Schema
let Ticket = new Schema(
  'Tickets',
  { ticketId: Number,
    eventId: Number })

module.exports = mongoose.model('Tickets', Ticket)

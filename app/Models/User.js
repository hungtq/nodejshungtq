const mongoose = require('mongoose')
const Schema = mongoose.Schema
const BcryptService = require('../Services/BcryptService')



const userSchema = new Schema({
  username: {
    type: String,
    require: [true, 'Please enter your username']
  },
  password: {
    type: String,
    require: [true, 'Please enter your password']
  }
})
// let hash = BcryptService.hashSync(password, salt)
// console.log(hash)
// userSchema.methods.generateHash = function(password) {
//     return BcryptService.hashSync(password, BcryptService.genSaltSync(10), null)
// }


userSchema.pre('save', async function save(next) {
    try {
        const user = this
        user.username = user.username && user.username.trim().toLowerCase()
        // hash password
        if (user.password)
            user.password = await BcryptService.hash(user.password)
        next()
    } catch (error) {
        next(error)
    }
})

module.exports = mongoose.model('User', userSchema)
//

'use strict'
const mongoose = require('mongoose')
const Schema = mongoose.Schema
let Event = new Schema(
  'Events',
  { eventId: Number,
    eventName: String,
    eventEnd: Date,
    eventStart: Date,
    eventAmount: Number,
    eventNote: String })

module.exports = mongoose.model('Events', Event)

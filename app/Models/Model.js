'use strict'
const { Model } = require('objection')
const Knex = require('knex')

  // const { mysql } = require('../../config/database')
const { mysql } = require('../../config/databaseMongo')

const knex = Knex({
  client: 'mysql',
  connection: mysql
})
Model.knex(knex)

module.exports = Model

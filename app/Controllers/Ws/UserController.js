
'use strict'

const UserRepository = require('../../Repositories/UserRepository')

class UserController {
  constructor () {
    this.userRepo = new UserRepository()
  }

  getUser (io, socket) {
    socket.on('getUser', async (data, fn) => {
      console.log(`Client: ${data}`)
      const user = await this.userRepo.getUser(data.userId)
      socket.emit('getUser', user)
    })
  }
}

module.exports = UserController


'use strict'

const jsonSuccess = require('../../Responses/JsonSuccess')
const jsonError = require('../../Responses/JsonError')
const asyncWrap = require('../../Middleware/Policies/async-wrap')

const jwtService = require('../../Services/JwtService')
const authRepository = require('../../Repositories/AuthRepository')

module.exports = {
  login: asyncWrap(async (req, res) => {
    const { username, password } = req.body
    let user = await authRepository.login(username, password)
    // let user =  await User.create({username: "hungtq1211@bap.jp", password: 'sd12d'})
    // let user
    if (!user) return jsonError(res, { error: 'USER_NOT_FOUND' })
    console.log('user login', user)
    const userToken = {
      username: user.username,
      _id: user._id
    }
    const token = await jwtService.generateUserToken(userToken)
    if (!token) return jsonError(res, { error: 'GEN_TOKEN_ERROR' })

    return jsonSuccess(res, { data: token })
  }),

  register: asyncWrap(async (req, res) => {
    const { username, password } = req.body
    let user = await authRepository.register(username, password)
    if (!user) return jsonError(res, { error: 'CAN_NOT_REGISTER' })
    return jsonSuccess(res, { data: user })
  }),
  showlistUser: asyncWrap(async (req, res) => {
    const { username, password } = req.body
    let user = await authRepository.showlist(username, password)
    if (!user) return jsonError(res, { error: 'GEN_TOKEN_ERROR' })
  })

}

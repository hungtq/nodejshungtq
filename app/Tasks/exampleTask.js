'use strict'

const CronJob = require('cron').CronJob

let isRunning = false
console.log(`Before job instantiation`)
const exampleJob = new CronJob({
  cronTime: '10 * * * * *',
  onTick: () => {
    const newDate = new Date()
    console.log(`Check every second: ${newDate} isRunning ${isRunning}`)

    if (!isRunning) {
      isRunning = true

      setTimeout(() => {
        console.log(`Long running onTick complete: ${new Date()}`)
        isRunning = false
      }, 3000)
      console.log(`setTimeout triggered: ${new Date()}`)
    }
  }
})
console.log(`After job instantiation`)

module.exports = exampleJob
